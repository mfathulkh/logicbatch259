package PR20nomer;

import java.util.Scanner;

public class soal2Asli {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan waktu dalam format 12 jam : ");
		String waktu = input.next();
		String[] str = waktu.split(":");

		int jam = Integer.parseInt(str[0]);

		String menit = str[1];
		String detikAmPm = str[2]; 
		String detik = str[2].substring(0, detikAmPm.length() - 2);
		String AmPm = str[2].substring(detikAmPm.length() - 2, detikAmPm.length());

		String waktuBaru = " ";

		if ((0 <= jam && jam < 12) && (AmPm.equalsIgnoreCase("Am"))) {
			waktuBaru = String.format("%02d", jam) + ":" + menit + ":" + detik;
		} else if ((0 <= jam && jam < 12) && (AmPm.equalsIgnoreCase("Pm"))) {
			waktuBaru = (12 + jam) + ":" + menit + ":" + detik;
		} else if ((jam == 12) && (AmPm.equalsIgnoreCase("Am"))) {
			waktuBaru = "00" + ":" + menit + ":" + detik;
		} else if ((jam == 12) && (AmPm.equalsIgnoreCase("Pm"))) {
			waktuBaru = jam + ":" + menit + ":" + detik;
		}
		System.out.print(waktuBaru);
	}

}
