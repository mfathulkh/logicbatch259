package day3;

import java.util.Scanner;

public class prno2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan banyaknya barisan : ");
		int banyak = input.nextInt();

		int nilaiPertama = 1;
		int nilaiKedua = 1;
		int nilaiKetiga = 1;
		int nilaiSelanjutnya;
		System.out.print(nilaiPertama + " " + nilaiKedua + " " + nilaiKetiga + " ");
		nilaiSelanjutnya = nilaiKedua + (2 * nilaiKetiga);
		System.out.print(nilaiSelanjutnya + " ");
		for (int i = 3; i < banyak; i++) {
			int pangkat = (int) Math.pow(2, (i - 3));
			nilaiSelanjutnya = nilaiSelanjutnya + (2 * pangkat);
			System.out.print(nilaiSelanjutnya + " ");
		}
	}

}
