package day3;

import java.util.Scanner;

public class LatInputString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		String s = "Xsis Academy";

		// memecah string menjadi per kata menjadi Array
		String[] tempSplit = s.split(" ");

		// memecah string dalam Array menjadi per karakter
		char[] tempArr = s.toCharArray();

		// string kosong untuk menjadikan array ke string
		String result = "";
		for (int i = 0; i < tempArr.length; i++) {
			result += tempArr[i];
		}
		System.out.println(result);

		// membandingkan karakter A dengan isian Array yang dibuat
		if (tempArr[5] == 'A') {
			System.out.println("true");
		} else {
			System.out.println("false");
		}
		
		System.out.println();
	}

}
