package day3;

import java.util.Scanner;

public class Prno1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan banyaknya barisan : ");
		int banyak = input.nextInt();

		int nilaiPertama = 1;
		int nilaiKedua = 1;
		int nilaiSelanjutnya;

		System.out.print(nilaiPertama + " " + nilaiKedua + " ");

		for (int i = 2; i < banyak; i++) {
			nilaiSelanjutnya = nilaiPertama + nilaiKedua;
			nilaiPertama = nilaiKedua;
			nilaiKedua = nilaiSelanjutnya;
			System.out.print(nilaiSelanjutnya + " ");
		}

	}

}
