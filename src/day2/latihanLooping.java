package day2;

import java.util.Scanner;

public class latihanLooping {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("masukkan nilai n : ");

		int n = input.nextInt();
		int x = 1;
		
		int[] tempArrayInteger = new int[5]; // array integer yang menampung 5 data
		
		String[] tempArrayString = new String[5]; // array string yang menampung 5 data
		
		int[] tempArray = new int[n]; // array dengan panjang n (sesuai inputan) 
		
		// mengisi nilai array
		for(int i = 0; i < tempArray.length; i++) {
			tempArray[i] = x;
			x++;
		}
		
		// mencetak array
		for (int i = 0; i < tempArray.length; i++) {
			System.out.print(tempArray[i] + " ");
		}
		
		
//		for (int i = 0; i < n; i++) {
//			x++; // ditambahkan nilai x sebelum dicetak
//			System.out.println(x + " ");
////			x++; ditambahkan nilai x setelah dicetak
//
////			if (i % 2 == 0) {
////				System.out.println(i + " "); kondisi dalam for
////			}
//		}
		
	}

}
