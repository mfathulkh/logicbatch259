package day2;

import java.util.Scanner;

public class Case01 {
	public static void Resolve1() {
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan nilai n = ");
		int n = input.nextInt();

		System.out.println("masukkan nilai n2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int x = 1;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else {
					tempArray[i][j] = x;
					x *= n2;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
	}

	public static void Resolve2() {
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan nilai n = ");
		int n = input.nextInt();

		System.out.println("masukkan nilai n2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int y = 1;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j % 3 == 2) {
					tempArray[i][j] = -y;
					System.out.print(tempArray[i][j] + " ");
					y = (y * n2);
				} else {
					tempArray[i][j] = y;
					System.out.print(tempArray[i][j] + " ");
					y = y * (n2);
				}
			}
			System.out.println();
		}
	}

	public static void Resolve3() {

		Scanner input = new Scanner(System.in);

		System.out.println("masukkan nilai n = ");
		int n = input.nextInt();

		System.out.println("masukkan nilai n2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int y = 3;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else {
					if (j < n / 2) {
						tempArray[i][j] = y;
						System.out.print(tempArray[i][j] + " ");
						y = y * 2;
					} else {
						tempArray[i][j] = y / 2;
						System.out.print(tempArray[i][j] + " ");
						y = (y / 2);
					}
				}
			}
			System.out.println();
		}
	}

	public static void Resolve4() {

		Scanner input = new Scanner(System.in);

		System.out.println("masukkan nilai n = ");
		int n = input.nextInt();

		System.out.println("masukkan nilai n2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int y = 1;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j % 2 == 1) {
					tempArray[i][j] = n2;
					System.out.print(tempArray[i][j] + " ");
					n2 = n2 + 5;
				} else {
					tempArray[i][j] = y;
					y = y + 1;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
	}

	public static void Resolve5() {
		Scanner input = new Scanner(System.in);

		System.out.println("masukkan nilai n = ");
		int n = input.nextInt();

		int[][] tempArray = new int[3][n];
		int y = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				tempArray[i][j] = y;
				System.out.print(tempArray[i][j] + " ");
				y = y + 1;
			}
			System.out.println();
		}

	}

	public static void Resolve6() {

		Scanner input = new Scanner(System.in);

		System.out.println("masukkan nilai n = ");
		int n = input.nextInt();

		int[][] tempArray = new int[3][n];

		int y = 1;
		int z = 1;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i < 2) {
					tempArray[i][j] = y;
					System.out.print(tempArray[i][j] + " ");
					y = y * 7;
				} else {
					z = (tempArray[0][j] + tempArray[1][j]);
					tempArray[i][j] = z;
					System.out.print(tempArray[i][j] + " ");
				}

			}
			System.out.println();
		}
	}

	public static void Resolve8() {

		Scanner input = new Scanner(System.in);

		System.out.println("masukkan nilai n = ");
		int n = input.nextInt();

		int[][] tempArray = new int[3][n];
		int y = 0;
		int z = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i < 2) {
					tempArray[i][j] = y;
					System.out.print(tempArray[i][j] + " ");
					y = (y + 2);
				} else {
					tempArray[i][j] = z;
					System.out.print(tempArray[i][j] + " ");
					z = (z + 3);
				}
			}
			System.out.println();
		}
	}

	public static void Resolve9() {

		Scanner input = new Scanner(System.in);

		System.out.println("masukkan nilai n = ");
		int n = input.nextInt();

		int[][] tempArray = new int[3][n];

		int y = 0;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i < 2) {
					tempArray[i][j] = y;
					System.out.print(tempArray[i][j] + " ");
					y = y + 3;
				} else {
					tempArray[i][j] = y - 3;
					System.out.print(tempArray[i][j] + " ");
					y = y - 3;
				}

			}
			System.out.println();
		}
	}

	public static void Resolve10() {

		Scanner input = new Scanner(System.in);

		System.out.println("masukkan nilai n = ");
		int n = input.nextInt();

		int[][] tempArray = new int[3][n];
		int y = 0;
		int z = 0;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i < 2) {
					tempArray[i][j] = y;
					y = y + 3;
					System.out.print(tempArray[i][j] + " ");
				} else {
					z = tempArray[0][j] + tempArray[1][j];
					System.out.print(z + " ");
				}
			}
			System.out.println();
		}

	}

	public static void Soal1() {
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan jumlah uang : ");
		int uang = input.nextInt();

		System.out.print("masukkan daftar harga baju : ");
		int[] daftarBaju = new int[4];
		for (int i = 0; i < daftarBaju.length; i++) {
			int daftar = input.nextInt();
			daftarBaju[i] = daftar;
		}
		System.out.print("masukkan daftar harga celana : ");
		int[] daftarCelana = new int[4];
		for (int j = 0; j < daftarCelana.length; j++) {
			int daftar = input.nextInt();
			daftarCelana[j] = daftar;
		}

		int total = 0;
		int max = 0;
		for (int i = 0; i < daftarBaju.length; i++) {
			total = daftarBaju[i] + daftarCelana[i];
			if (total <= uang && total > max) {
				max = total;
			}
		}
		System.out.println("harga yang dapat Andi beli sebesar : " + max + " ");
	}

//				int[] arrayHasil = new int[100];
//				x=arrayHasil[i];
//				y=arrayHasil[i];
//				for (int k = 0; k < arrayHasil.length; k++) {
//					if (arrayHasil[k] < arrayHasil[k + 1]) {
//						System.out.println(arrayHasil[k + 1]);
//					} else {
//						System.out.println(arrayHasil[k]);
//					}
//				}

	public static void Soal3() {
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan jumlah puntung rokok yang anda peroleh : ");
		int a = input.nextInt();

		int b = 1;
		int sisa = 0;
		int uang = 0;
		if (a % 8 == 0) {
			sisa = 0;
			b = a / 8;
			System.out.println("jumlah rokok yang dapat dirangkai adalah : " + b);
		} else {
			sisa = a % 8;
			b = (a - sisa) / 8;
			System.out.println("jumlah rokok yang dapat dirangkai adalah : " + b);
		}
		System.out.println();
		uang = 500 * b;
		System.out.print("anda mendapatkan uang sebesar : " + uang);
	}

	public static void Soal4() {
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan jumlah uang yang elsa miliki : ");
		int uang = input.nextInt();
		System.out.println("masukkan jumlah makanan yang akan dibeli : ");
		int jumlah = input.nextInt();

		int[] arrayHarga = { 12000, 20000, 9000, 15000 };
		System.out.println("Daftar harga makanan : ");
		for (int i = 0; i < arrayHarga.length; i++) {
			System.out.println(arrayHarga[i] + " ");
		}

		int totalHarga = 0;
		int jumlahBayar;
		int jumlahBayarElsa;
		int sisa;

		System.out.println();
		System.out.println("masukkan index makanan yang tidak bisa elsa makan : ");
		int index = input.nextInt();
		for (int j = 0; j < arrayHarga.length; j++) {
			totalHarga = totalHarga + arrayHarga[j];
		}
		jumlahBayar = totalHarga - arrayHarga[index];
		jumlahBayarElsa = jumlahBayar / 2;
		sisa = uang - jumlahBayarElsa;
		if (sisa == 0) {
			System.out.println("uang elsa pas ");
		} else {
			System.out.println("uang elsa kurang " + sisa);
		}
		System.out.println("total harga : " + totalHarga);
		System.out.println("jumlah yang harus elsa bayarkan adalah : " + jumlahBayarElsa);
		System.out.println("sisa uang elsa adalah : " + sisa);
		System.out.println();
	}

	public static void soal5() { // belumsemua
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan sebuah kalimat : ");
		String kalimat = input.nextLine();
//		String[] kalimatSplit = kalimat.split(" ");
		char[] tempChar = kalimat.toCharArray();

		char[] huruf = { 'a', 'i', 'u', 'e', 'o' };
		String vokal = "";
		String konsonan = "";
		for (int i = 0; i < tempChar.length; i++) {
			for (int j = 0; j < huruf.length; j++) {
				if (tempChar[i] == huruf[j]) {
					vokal = vokal + tempChar[i];
				} else {
					konsonan = konsonan + tempChar[i];
				}
			}
		}
		System.out.println("vokal " + vokal);
		System.out.println("konsonan " + konsonan);
//		for (int i = 0; i < (kalimatSplit.length); i++) {
//			for (int j = 0; j < tempChar.length; j ++) {
//				if (tempChar[j] == huruf[i]) {
//					System.out.print("vokal = " + " " + huruf[i] + " ");
//				} else {
//					System.out.print("konsonan = " + " " + tempChar[j] + " ");
//				}
//			} System.out.println();
//		}
	}

	public static void soal2() {
		Scanner input = new Scanner(System.in);
		int[] nilai = new int[5];
		System.out.println("masukkan nilai-nilai ke dalam array : ");
		for (int i = 0; i < nilai.length; i++) {
			int nilai1 = input.nextInt();
			nilai[i] = nilai1;
		}

		System.out.println("masukkan banyaknya rotasi : ");
		int rotasi = input.nextInt();

		if (rotasi < nilai.length) {
			for (int i = 0; i < rotasi; i++) {
				nilai[i] = nilai[i + rotasi];
				System.out.println(nilai[i]);
			}
		}

	}
	
	public static void soal6() {
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan sebuah kalimat : ");
		String Kalimat = input.nextLine();
		String[] Kalimat1 = Kalimat.split(" ");
		String result = " ";
		 for (int i=0; i<Kalimat1.length; i++) {
			 char[] Kata = Kalimat1[i].toCharArray();
			 for (int j=0; j<Kata.length; j++) {
				 if (j%2==0) {
					 result = result + "*";
				 } else {
					 result = Kata[j];
				 }
				 
			 }
			 result = result + " ";
		 }
		 System.out.println(result);
	}
}