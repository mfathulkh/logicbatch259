package day1;

import java.util.Scanner;

public class Logic1 {
	// materi tipe data nih
	// tipe data tertentu harus diisi data yang sesuai.
int id=1; // int tipe data primitive. kalau Int (bukan primitif)
String nama = "fathul";
int a = 1;
double b = 1.23;
int[] array1= {1,2,3,4,5}; // array menyimpan banyak data dalam satu variabel.
boolean salah = false;

	public static void main(String[] args) {
		
		Logic1 fath = new Logic1();
		System.out.println("nama :" + fath.nama);
		System.out.println("Hello World");
		System.out.println(" " +fath.salah);
		// TODO Auto-generated method stub
		
		String seribu = "1000";
		String duaribu = "2000";
		System.out.println(seribu + duaribu); // ini String dijumlahkan String
		// kalau String dijumlahkan nanti tidak bisa dijumlahkan
		
		//kalau ingin mengganti tipe data dari string ke integer dikasi Integer.parseint(nama variabel) (kaya dibawah))
		//kalo tipe datanya double tinggal ganti double.parsedouble(nama variabel) dst..
		System.out.println(Integer.parseInt(seribu) + Integer.parseInt(duaribu));
		// belajar input
		Scanner input = new Scanner(System.in); // untuk memasukkan nilai dalam console..
		System.out.println("masukkan nilai 1: ");
		int numberOne = input.nextInt(); // memasukkan nilai int
		System.out.println("masukkan nilai 2:"); 
		int numberTwo = input.nextInt();
		int jumlah = numberOne + numberTwo; // langsung dibuatkan variabel untuk hasil penjumlahannya
		System.out.println("hasil penjumlahan nilai nomer satu + nomer dua adalah : " + (numberOne+numberTwo));
		System.out.println("jumlah = " + jumlah);
		System.out.println("tuliskan nama kamu : ");
		String namaKamu = input.next(); // memasukkan nilai String
		System.out.println("nama kamu adalah : " + namaKamu);
		
	}

}
