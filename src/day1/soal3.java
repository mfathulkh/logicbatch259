package day1;

import java.util.Scanner;

public class soal3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		System.out.println("Silahkan masukkan harga barang yang anda order : ");
		int order = input.nextInt();
		System.out.println("Silahkan masukkan harga ongkir anda : ");
		int ongkir = input.nextInt();
		int potongan = 0;
		int diskonOngkir = 0;

		if (order < 30000) {
			System.out.println("Maaf anda tidak mendapatkan free ongkir dan potongan belanja");
		} else if (order >= 30000 && order < 50000) {
			diskonOngkir = 5000;
			potongan = 5000;
			if (ongkir < 50000) {
				diskonOngkir = ongkir;
			}
			System.out.println("Selamat anda mendapatkan free ongkir sebesar : " + diskonOngkir
					+ " dan mendapatkan potongan harga sebesar : " + potongan);
		} else if (order >= 50000 && order < 100000) {
			diskonOngkir = 10000;
			potongan = 10000;
			if (ongkir < 10000) {
				diskonOngkir = ongkir;
			}
			System.out.println("Selamat anda mendapatkan free ongkir sebesar : " + diskonOngkir
					+ " dan mendapatkan potongan harga sebesar : " + potongan);

		} else {
			diskonOngkir = 10000;
			potongan = 20000;
			if (ongkir < 10000) {
				diskonOngkir = ongkir;
			}
			System.out.println("Selamat anda mendapatkan free ongkir sebesar : " + diskonOngkir
					+ " dan mendapatkan potongan harga sebesar : " + potongan);
		}
		System.out.println("Berikut adalah rekap harga belanja anda : ");
		System.out.println("Harga barang yang anda order sebesar : " + order);
		System.out.println("Ongkir anda sebesar : " + ongkir);
		System.out.println("Diskon ongkir yang anda peroleh sebesar : " + diskonOngkir);
		System.out.println("Potongan harga yang anda peroleh ");
		System.out.println("Total harga yang harus anda bayarkan sebesar : " + (order + ongkir - potongan - diskonOngkir));

	}

}
