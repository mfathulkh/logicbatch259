package day4;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Case01 {

	public static void Resolve1() {

		Scanner s = new Scanner(System.in);
		System.out.println("Jam Masuk : ");
		String masuk = s.next();
		String jamMasuk = masuk.substring(0, 2);
		String menitMasuk = masuk.substring(3);

		System.out.println("Keluar Gedung pada Jam : ");
		String keluar = s.next();
		String jamKeluar = keluar.substring(0, 2);
		String menitKeluar = keluar.substring(3);

		int jamMasuk1 = Integer.parseInt(jamMasuk);
		int menitMasuk1 = Integer.parseInt(menitMasuk);
		int jamKeluar1 = Integer.parseInt(jamKeluar);
		int menitKeluar1 = Integer.parseInt(menitKeluar);

		int waktu = (3600 * jamMasuk1) + (60 * menitMasuk1);
		int waktu1 = (3600 * jamKeluar1) + (60 * menitKeluar1);

		int detik = waktu - waktu1;
		int jammm = detik / 3600;
		int sisa = detik % 3600;
		int menit = sisa / 60;

		System.out.println("waktu parkir = " + jammm + " jam" + menit + " menit");

		if (menit > 30) {
			jammm += 1;
		}
		int tarif = jammm * 3000;
		System.out.println(tarif);

	}

	public static void Resolve2() {

		// versimanual
		Scanner input = new Scanner(System.in);
//		System.out.println("masukkan tanggal anda meminjam buku : ");
//		String tanggalPinjam = s.nextLine();
//		String[] pinjam = tanggalPinjam.split("-");
//		System.out.println("masukkan tanggal anda mengembalikan buku : ");
//		String tanggalKembali = s.nextLine();
//		String[] kembali = tanggalKembali.split("-");
//
//		int tanggal = Integer.parseInt(pinjam[0]);
//		int bulan = Integer.parseInt(pinjam[1]);
//		int tahun = Integer.parseInt(pinjam[2]);
//
//		int tanggal1 = Integer.parseInt(kembali[0]);
//		int bulan1 = Integer.parseInt(kembali[1]);
//		int tahun1 = Integer.parseInt(kembali[2]);
//
//		int selisihBulan = bulan1 - bulan;
//		int selisihTanggal = (tanggal1 - tanggal) + selisihBulan * 30;
//
//		int hitung = 0;
//		int denda = 0;
//		for (int i = 0; i < selisihTanggal - 3; i++)
//			if (selisihTanggal > 3) {
//				hitung = hitung + 1;
//			}
//		denda = hitung * 500;
//
//		System.out.println(selisihTanggal);
//		System.out.println(selisihBulan);
//		System.out.println(denda);

		// versiotomatis

		System.out.println("Tanggal Peminjaman :");
		String pinjam = input.nextLine();
		System.out.println("\nTanggal Pengembalian : ");
		String kembali = input.nextLine();

		SimpleDateFormat minjam = new SimpleDateFormat("dd-MM-yyyy");

		Date peminjaman = null;
		Date pengembalian = null;

		try {
			peminjaman = minjam.parse(pinjam);
			pengembalian = minjam.parse(kembali);
			long selisih = pengembalian.getTime() - peminjaman.getTime();
			long selisihTanggal = selisih / (24 * 60 * 60 * 1000);
			if (selisihTanggal > 3) {
				int telat = (int) selisihTanggal - 3;
				int denda = telat * 500;
				System.out.println("\n Denda anda sebesar = Rp. " + denda);
			} else {
				System.out.println("anda tidak telat ");
			}
		} catch (ParseException x) {
			x.printStackTrace();
		}

	}

	public static void Resolve3() {

		Scanner s = new Scanner(System.in);

		System.out.println("masukkan tanggal masuk : ");
		String inputTanggalMulai = s.nextLine();

		System.out.println("masukkan lama bootcamp : ");
		int lama = s.nextInt();
		s.nextLine();

		System.out.println("masukkan tanggal libur : ");
		String[] libur = s.nextLine().split(",");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

		String nextDate = "";
		try {

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateFormat.parse(inputTanggalMulai));

			int hitung = 0;
			int tempHitungLibur = 0;

			int i = 0;
			while (i < lama + tempHitungLibur) {
				calendar.add(Calendar.DATE, 1);
				int day = calendar.getTime().getDay();
				int dateofHoliday = calendar.getTime().getDate();

				int j = 0;
				if ((day == 6) || (day == 0)) {
					tempHitungLibur++;
				}
				while (j < libur.length) {
					if (dateofHoliday == Integer.parseInt(libur[j])) {
						tempHitungLibur++;
					}
					j++;
				}
				i++;
			}

			System.out.println("total libur = " + (tempHitungLibur + libur.length));
			System.out.println(hitung);
			nextDate = dateFormat.format(calendar.getTime());
			System.out.println(nextDate);

		} catch (ParseException x) {
			x.printStackTrace();
		}
	}

	public static void Resolve4() {
		Scanner s = new Scanner(System.in);

		System.out.println("masukkan sebuah kalimat : ");
		String kalimat = s.nextLine();

		char[] kalimat1 = kalimat.toCharArray();
		String balikKata = "";
		for (int i = 0; i < kalimat.length() + 1; i++)
			if (kalimat1[i] == kalimat1[i + (kalimat.length() - 1)]
					&& kalimat1[i + 1] == kalimat1[i + (kalimat.length() - 2)]) {
//				balikKata = balikKata + kalimat1[kalimat.length()-i];
				System.out.println("yes");
			} else {
				System.out.println("no");
			}
		System.out.println(balikKata);
	}


	public static void Resolve5() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("masukkan banyak data : ");
		int banyakData = input.nextInt();

		int[] data = new int[banyakData];
		System.out.println("masukkan data : ");
		for (int i = 0; i < banyakData; i++) {
			data[i] = input.nextInt();
		}

		Arrays.sort(data);

		System.out.println("data setelah diurutkan = " + Arrays.toString(data));

		double jumlah = 0;
		double ratarata = 0;
		int medianGanjil = 0;
		double medianGenap = 0;
		int modus = 0;
		int maxHitung = 0;
		for (int i = 0; i < data.length; i++) {
			jumlah = jumlah + data[i];
			if (banyakData % 2 != 0) {
				medianGanjil = data[(data.length / 2)];
			} else if (banyakData % 2 == 0) {
				medianGenap = (double) data[((data.length) / 2) - 1] / 2 + (double) data[((data.length) / 2)] / 2;
			}
		}
		for (int i = 0; i < data.length; i++) {
			int hitung = 0;
			for (int j = 0; j < data.length; j++) {
				if (data[j] == data[i])
					hitung++;
			}
			if (hitung > maxHitung) {
				maxHitung = hitung;
				modus = data[i];
			}
		}

		ratarata = jumlah / banyakData;

		System.out.println("jumlah = " + (int) jumlah);
		System.out.println("rata-rata =  " + ratarata);
		System.out.println("median data ganjil  = " + medianGanjil);
		System.out.println("median data genap = " + medianGenap);
		System.out.println("modus = " + modus);
	}
}
